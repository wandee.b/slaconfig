package com.api.slaservice;

import org.springframework.http.HttpStatus;

public class CustomException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private String message = "";
    private HttpStatus httpStatus = null;
    private String customStatus = "";

    public CustomException(String entity, Long id) {
        // super(entity+" does not exist for key_id =>"+id);
        this.message = entity + " does not exist for key_id =>" + id;
        //this.httpStatus = HttpStatus.NOT_FOUND;
        this.customStatus = HttpStatus.NOT_FOUND + "";
    }

    public CustomException(String entity) {
        // super(entity+" does not exist");
        this.message = entity + " does not exists";
        // this.httpStatus = HttpStatus.NOT_FOUND;
        this.customStatus = HttpStatus.NOT_FOUND + "";
    }

    // public CustomException(String message, HttpStatus httpStatus) {
    //     this.message = message;
    //     this.httpStatus = httpStatus;
    // }

    public CustomException(String message, String customStatus) {
        this.message = message;
        this.customStatus = customStatus;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getLocalizedMessage(){
        return customStatus;
    }

}
