package com.api.slaservice;

import java.util.Collections;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage( "com.api.slaservice" ))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(getAppInformation())
                .useDefaultResponseMessages(false);
//                .useDefaultResponseMessages(false)
//                .globalResponseMessage(RequestMethod.GET,
//                        newArrayList(new ResponseMessageBuilder()
//                                .code(500)
//                                .message("Unsuccessfully")
//                                .responseModel(new ModelRef("Error"))
//                                .build(),
//                                new ResponseMessageBuilder()
//                                        .code(200)
//                                        .message("Successfully")
//                                        .build()));
    }
    private ApiInfo getAppInformation(){
        
        return new ApiInfo("SLA REST API","","1.0","",new Contact("","","")
                ,"","",Collections.emptyList());
    }
}
