package com.api.slaservice.Models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Document("sla")
public class SLA{

    @Id
    public ObjectId _id;
    public String module;
    public String name;
    public String description;
    public Object operation_time;
    public String appply_policy_to;
    public SLACondition condition;
    public SLAGoal sla_goal;
    public SLAMeasurement measurement;
    public SLANotify notify;

    public String get_id() {

        return _id.toHexString();

    }

}