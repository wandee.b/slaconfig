package com.api.slaservice.Models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SLATemp {

    @Id
    public ObjectId _id;
    public String module;
    public String name;
    public String description;
    public String operation_time;
    public String apply_policy_to;
    public SLATempTarget sla_target[];
    public SLATempMeasurement measurement[];
    public SLATempNotify notify[];

    public String get_id() {

        return _id.toHexString();

    }

}
