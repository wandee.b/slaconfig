package com.api.slaservice.Models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SLATempTarget {

    public String sla_goal;
    public String cri_val;
    public String cri_time;
    public String high_val;
    public String high_time;
    public String medium_val;
    public String medium_time;
    public String low_val;
    public String low_time;
    public String category;

}
