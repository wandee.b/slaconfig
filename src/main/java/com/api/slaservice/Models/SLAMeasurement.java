package com.api.slaservice.Models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SLAMeasurement {

    public String start;
    public String stop;
    public String pause;
    public String sla_goal;

}
