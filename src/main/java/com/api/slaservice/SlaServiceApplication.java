package com.api.slaservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SlaServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SlaServiceApplication.class, args);
	}

}
