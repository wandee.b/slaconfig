package com.api.slaservice.Controllers;

import java.util.List;

import com.api.slaservice.CustomException;
import com.api.slaservice.Models.SLA;
import com.api.slaservice.Models.SLACondition;
import com.api.slaservice.Models.SLAGoal;
import com.api.slaservice.Models.SLAMeasurement;
import com.api.slaservice.Models.SLANotify;
import com.api.slaservice.Models.SLATemp;
import com.api.slaservice.Repositories.SLARepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.http.HttpStatus;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/sla")
public class SLAController {

    @Autowired
    private SLARepository Repository;

    // query
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value="All priority")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<SLA> getAllSLA() {

        return Repository.findAll();

    }

    private void sla(String module, String name, String desc, String operTime
    , String apply, String priority, String match, String category
    ,String slaGoal, String time, String start
    , String stop, String pause, Object notify) {

        SLA s = new SLA();
        SLACondition c = new SLACondition();
        SLAGoal g = new SLAGoal();
        SLAMeasurement m = new SLAMeasurement();
        SLANotify n = new SLANotify();

        s.module = module;
        s.name = name;
        s.description = desc;
        s.operation_time = operTime;
        s.appply_policy_to = apply;

        c.priority = priority;
        c.match = match;
        c.category = category;
        s.condition = c;

        g.goal = slaGoal;
        g.time = time;
        s.sla_goal = g;

        m.sla_goal = slaGoal;
        m.start = start;
        m.stop = stop;
        m.pause = pause;
        s.measurement = m;

        n.sla_goal = slaGoal;
        n.time_notify = notify;
        s.notify = n;

        ObjectId oid = new ObjectId();
        s._id = oid;
        Repository.save(s);

    }

    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value="Create priority")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public void createSLA(@RequestBody SLATemp s) {

        for (int i = 0; i < s.sla_target.length; i++) {

            if (s.sla_target[i].cri_val != "") {

                sla(s.module
                    ,s.name
                    ,s.description
                    ,s.operation_time
                    ,s.apply_policy_to
                    ,"Critical"
                    ,"" //match
                    ,"" //category
                    ,s.sla_target[i].sla_goal
                    ,s.sla_target[i].cri_val
                    ,s.measurement[i].start
                    ,s.measurement[i].stop
                    ,s.measurement[i].pause
                    ,s.notify[i].time_notify);
            }
            if (s.sla_target[i].high_val != "") {

                sla(s.module
                    ,s.name
                    ,s.description
                    ,s.operation_time
                    ,s.apply_policy_to
                    ,"High"
                    ,"" //match
                    ,"" //category
                    ,s.sla_target[i].sla_goal
                    ,s.sla_target[i].high_val
                    ,s.measurement[i].start
                    ,s.measurement[i].stop
                    ,s.measurement[i].pause
                    ,s.notify[i].time_notify);
            }
            if (s.sla_target[i].medium_val != "") {

                sla(s.module
                    ,s.name
                    ,s.description
                    ,s.operation_time
                    ,s.apply_policy_to
                    ,"Medium"
                    ,"" //match
                    ,"" //category
                    ,s.sla_target[i].sla_goal
                    ,s.sla_target[i].medium_val
                    ,s.measurement[i].start
                    ,s.measurement[i].stop
                    ,s.measurement[i].pause
                    ,s.notify[i].time_notify);
            }
            if (s.sla_target[i].low_val != "") {

                sla(s.module
                    ,s.name
                    ,s.description
                    ,s.operation_time
                    ,s.apply_policy_to
                    ,"Low"
                    ,"" //match
                    ,"" //category
                    ,s.sla_target[i].sla_goal
                    ,s.sla_target[i].low_val
                    ,s.measurement[i].start
                    ,s.measurement[i].stop
                    ,s.measurement[i].pause
                    ,s.notify[i].time_notify);
            }
        }
        throw new CustomException("OK", "200"); 

    }
}