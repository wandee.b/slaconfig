package com.api.slaservice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(CustomException.class)
    public ResponseEntity<CustomExceptionResponse> customHandleNotFound(Exception ex, WebRequest request) {

        CustomExceptionResponse errors = new CustomExceptionResponse();
        errors.setTimestamp(LocalDateTime.now());
        errors.setMessage(ex.getMessage());
        // errors.setStatus(HttpStatus.NOT_FOUND.value());
        errors.setStatus(ex.getLocalizedMessage());

        return new ResponseEntity<>(errors, HttpStatus.NOT_FOUND);

    }
}
