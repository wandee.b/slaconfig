package com.api.slaservice.Repositories;

import com.api.slaservice.Models.SLA;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface SLARepository extends MongoRepository<SLA, String> {
    
    // @Query("{'role.title': ?0}")
    // List<users> findByRole(String title);

}